import 'package:abd_samakeh_test/features/user/presentation/bloc/delete_user_cubit/delete_user_cubit.dart';
import 'package:abd_samakeh_test/features/user/presentation/bloc/update_user_cubit/update_user_cubit.dart';
import 'package:abd_samakeh_test/features/user/presentation/bloc/user_cubit/user_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../core/errorPage/error_screen.dart';

import '../features/home/presentation/pages/home_page.dart';
import '../features/user/domain/entities/response/user_entite.dart';
import '../features/user/presentation/bloc/save_cubit/save_user_cubit.dart';
import '../features/user/presentation/pages/create_user_page.dart';
import '../features/user/presentation/pages/update_user_page.dart';
import '../injection/injection_container.dart' as di;

class AppRouter {
  Route? onGenerateRoute(RouteSettings settings) {
    final argument = settings.arguments;
    switch (settings.name) {
      //Home Feature
      //Home Screen
      case RouteNamedScreens.homePageNameRoute:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => di.sl<UserCubit>()..getUsers(),
              ),
              BlocProvider(create: (context) => di.sl<DeleteUserCubit>())
            ],
            child: const HomePage(),
          ),
        );
      //
      //User Feature
      //Create User Screen
      case RouteNamedScreens.createUserPageNameRoute:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => di.sl<SaveUserCubit>(),
              )
            ],
            child: const CreateUserPage(),
          ),
        );
         //Create User Screen
      case RouteNamedScreens.editUserPageNameRoute:
      argument as User;
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => di.sl<UpdateUserCubit>(),
              )
            ],
            child:  UpdateUserPage(userDetails:argument ),
          ),
        );

      default:
        return MaterialPageRoute(builder: (_) => const NotFoundScreen());
    }
  }
}

class RouteNamedScreens {
  //! Important Constant
  // Routing Naming
  static const String homePageNameRoute = '/';
  static const String createUserPageNameRoute = '/create-user-page-route';
  static const String editUserPageNameRoute = '/edit-user-page-route';
}
