import 'package:abd_samakeh_test/features/user/presentation/bloc/delete_user_cubit/delete_user_cubit.dart';
import 'package:abd_samakeh_test/features/user/presentation/bloc/save_cubit/save_user_cubit.dart';
import 'package:abd_samakeh_test/features/user/presentation/bloc/update_user_cubit/update_user_cubit.dart';
import 'package:abd_samakeh_test/features/user/presentation/bloc/user_cubit/user_cubit.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../core/network/network_info.dart';
import '../features/user/domain/repositories/user_repository.dart';
import '../features/user/domain/usecases/delete_user_usecase.dart';
import '../features/user/domain/usecases/get_users_usecase.dart';
import '../features/user/domain/usecases/save_user_usecase.dart';
import '../features/user/domain/usecases/update_user_usecase.dart';

final sl = GetIt.instance;

Future<void> init() async {
//! Features - User
// Bloc
  sl.registerFactory(() => UserCubit(getUsersUsecase: sl()));
  sl.registerFactory(() => SaveUserCubit(saveUserUsecase: sl()));
  sl.registerFactory(() => DeleteUserCubit(deleteUserUsecase: sl()));
  sl.registerFactory(() => UpdateUserCubit(updateUserUsecase: sl()));
// Usecase
  sl.registerLazySingleton(() => GetUsersUsecase(userRepository: sl()));
  sl.registerLazySingleton(() => SaveUserUsecase(userRepository: sl()));
  sl.registerLazySingleton(() => DeleteUserUsecase(userRepository: sl()));
  sl.registerLazySingleton(() => UpdateUserUsecase(userRepository: sl()));
// Repo
  sl.registerLazySingleton(() => UserRepository());
// DataSources
  // Remote
  // Local
//

//! Features - Auth Finished

//! Core

  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImplemntes(sl()));

//! External
  sl.registerLazySingleton(() => Client());

  sl.registerLazySingleton(() => InternetConnectionChecker());
}
