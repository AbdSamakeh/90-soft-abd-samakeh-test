// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'localization_bloc.dart';

abstract class LocalizationEvent extends Equatable {
  const LocalizationEvent();

  @override
  List<Object> get props => [];
}

class ChangeLanguage extends LocalizationEvent {
  final String language;
  const ChangeLanguage({
    required this.language,
  });

  ChangeLanguage copyWith({
    String? language,
  }) {
    return ChangeLanguage(
      language: language ?? this.language,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is ChangeLanguage &&
      other.language == language;
  }

  @override
  int get hashCode => language.hashCode;
}
