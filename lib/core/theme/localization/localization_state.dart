part of 'localization_bloc.dart';

enum LocalizationStates {
  toggleLanguage,
  initial,
}

class LocalizationState extends Equatable {
  final LocalizationStates states;
  final String language;
  const LocalizationState({required this.states, required this.language});

  factory LocalizationState.initial() {
  
    return LocalizationState(
        states: LocalizationStates.initial,
        language: AppSharedPreferences.getLanguage());
  }

  LocalizationState copyWith({LocalizationStates? states, String? language}) {
    return LocalizationState(
        states: states ?? this.states, language: language ?? this.language);
  }

  @override
  List<Object> get props => [states, language];
}
