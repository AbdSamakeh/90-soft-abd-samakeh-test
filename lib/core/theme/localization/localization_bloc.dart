import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../shared/shared_pref.dart';

part 'localization_event.dart';
part 'localization_state.dart';

class LocalizationBloc extends Bloc<LocalizationEvent, LocalizationState> {
  LocalizationBloc() : super(LocalizationState.initial()) {
    on<ChangeLanguage>(
      (event, emit) {
        emit(
          state.copyWith(
              states: LocalizationStates.toggleLanguage,
              language: event.language),
        );
        AppSharedPreferences.cashLanguage(language: event.language);
      },
    );
  }
}
