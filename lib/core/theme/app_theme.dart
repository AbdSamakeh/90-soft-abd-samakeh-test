import 'package:flutter/material.dart';

import '../resources/color_manger.dart';

const primaryColor = AppColorsManger.mainAppColor;
const secondryColor = AppColorsManger.black;
ThemeData lightTheme() {
  return ThemeData(
    appBarTheme: const AppBarTheme(
      backgroundColor: primaryColor,
      centerTitle: true,
    ),
    brightness: Brightness.light,
    primaryColorLight: AppColorsManger.mainAppColor,
    primaryColorDark: AppColorsManger.mainAppColor,
    scaffoldBackgroundColor: AppColorsManger.white,
    splashColor: AppColorsManger.white,
    primaryColor: primaryColor,
    progressIndicatorTheme:
        const ProgressIndicatorThemeData(color: primaryColor),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: primaryColor, foregroundColor: secondryColor),
    inputDecorationTheme: InputDecorationTheme(
      floatingLabelStyle: const TextStyle(
        color: primaryColor,
      ),
      iconColor: secondryColor,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: primaryColor),
        borderRadius: BorderRadius.circular(12),
      ),
      border: OutlineInputBorder(
        borderSide: const BorderSide(color: primaryColor),
        borderRadius: BorderRadius.circular(12),
      ),
    ),
    tabBarTheme: TabBarTheme(
      indicator: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.circular(12),
      ),
    ),
    colorScheme: const ColorScheme.light(primary: primaryColor)
        .copyWith(secondary: AppColorsManger.mainAppColor)
        .copyWith(secondary: AppColorsManger.mainAppColor),
  );
}

ThemeData darkTheme() {
  return ThemeData(
    appBarTheme: const AppBarTheme(
      backgroundColor: primaryColor,
      centerTitle: true,
    ),
    splashColor: AppColorsManger.white,
    brightness: Brightness.dark,
    primaryColorDark: AppColorsManger.mainAppColor,
    primaryColor: primaryColor,
    progressIndicatorTheme:
        const ProgressIndicatorThemeData(color: primaryColor),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: primaryColor, foregroundColor: secondryColor),
    inputDecorationTheme: InputDecorationTheme(
      floatingLabelStyle: const TextStyle(
        color: primaryColor,
      ),
      iconColor: secondryColor,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: primaryColor),
        borderRadius: BorderRadius.circular(12),
      ),
      border: OutlineInputBorder(
        borderSide: const BorderSide(color: primaryColor),
        borderRadius: BorderRadius.circular(12),
      ),
    ),
    tabBarTheme: TabBarTheme(
      indicator: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.circular(12),
      ),
    ),
    colorScheme: const ColorScheme.dark(primary: primaryColor)
        .copyWith(secondary: AppColorsManger.mainAppColor)
        .copyWith(secondary: AppColorsManger.mainAppColor),
  );
}
