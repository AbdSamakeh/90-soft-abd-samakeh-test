// part of 'theme_bloc.dart';
// enum AppThemeStates {
//     light,
//   dark;

//   String toJson() => name;

//   static AppThemeStates fromJson(String json) => values.byName(json);
// }



// class AppThemeState extends Equatable {
//   final AppThemeStates appTheme;
//   const AppThemeState({
//    required this.appTheme,
//   });

//   factory AppThemeState.initial() {
//     return const AppThemeState(appTheme: AppThemeStates.light);
//   }

//   @override
//   List<Object> get props => [appTheme];

//   @override
//   String toString() => 'ThemeState(appTheme: $appTheme)';

//   AppThemeState copyWith({
//     AppThemeStates? appTheme,
//   }) {
//     return AppThemeState(
//       appTheme: appTheme ?? this.appTheme,
//     );
//   }

//   Map<String, dynamic> toJson() {
//     final result = <String, dynamic>{};

//     result.addAll({'appTheme': appTheme.toJson()});

//     return result;
//   }

//   factory AppThemeState.fromJson(Map<String, dynamic> json) {
//     return AppThemeState(
//       appTheme: AppThemeStates.fromJson(json['appTheme']),
//     );
//   }
// }
