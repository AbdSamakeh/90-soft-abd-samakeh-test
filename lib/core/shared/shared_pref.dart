// ignore_for_file: file_names

import 'package:shared_preferences/shared_preferences.dart';

import '../resources/keys_manger.dart';

class AppSharedPreferences {
  static late SharedPreferences _sharedPreferences;

  static init(SharedPreferences sh) {
    _sharedPreferences = sh;
  }

  static clear() {
    _sharedPreferences.clear();
  }

  // Save languge
  static cashLanguage({required String language}) {
    _sharedPreferences.setString(AppKeysManger.LANGUAGE, language);
  }

  // Get Language
  static String getLanguage() {
    return _sharedPreferences.getString(AppKeysManger.LANGUAGE) ?? "en";
  }
}
