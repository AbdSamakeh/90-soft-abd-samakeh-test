import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../resources/color_manger.dart';

class TextUtils extends StatelessWidget {
  final String text;
  final double fontSize;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final Color? color;
  final TextDecoration? uderline;

  final TextAlign? textAlign;
  const TextUtils({
    Key? key,
    required this.text,
    required this.fontSize,
    this.fontWeight,
    this.color = AppColorsManger.black,
    this.uderline,
    this.fontStyle,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      softWrap: false,
      textAlign: textAlign,
      style: GoogleFonts.cairo(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: color,
        fontStyle: fontStyle,
        decoration: uderline,
      ),
    );
  }
}
