import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../resources/color_manger.dart';
import '../textUtils/text_utils.dart';

class NoteMessage {
  static showSnackBar(
      {required BuildContext context,
      required String text,
      int? duration,
      required bool warning}) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: SizedBox(
        height: 3.h,
        width: 100.w,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: TextUtils(
                text: text,
                fontSize: 14.sp,
                color: warning
                    ? AppColorsManger.red
                    : AppColorsManger.mainAppColor,
                fontWeight: FontWeight.bold,
              ),
            ),
            GestureDetector(
              onTap: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
              },
              child: Icon(
                Icons.cancel,
                color: warning
                    ? AppColorsManger.red
                    : AppColorsManger.mainAppColor,
                size: 5.w,
              ),
            )
          ],
        ),
      ),
      backgroundColor: Colors.white,
      duration: Duration(seconds: duration ?? 3),
      width: 90.w,
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: BorderSide(
          width: 2,
          color: warning ? AppColorsManger.red : AppColorsManger.mainAppColor,
        ),
      ),
    ));
  }
}
