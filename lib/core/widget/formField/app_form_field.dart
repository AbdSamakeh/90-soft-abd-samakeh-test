import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../resources/color_manger.dart';

class AppTextFormField extends StatelessWidget {
  final Widget? suffixIcon;
  final TextEditingController? controller;
  final bool? obscureText;

  final String? Function(String?)? validator;
  final String? Function(String?)? onFilledSubmited;
  final Function()? editingComplete;
  final String? Function(String?)? onChanged;
  final TextInputType? textInputType;
  final TextInputAction? textInputAction;

  final FocusNode? focusNode;
  final String? labelText;
  final String? initialValue;
  final Color? textColor;
  final Color? labelColor;
  final int? maxLines;
  final Widget? prefixIcon;
  final String? hintText;
  final bool? outlinedBorder;

  const AppTextFormField(
      {Key? key,
      this.suffixIcon,
      this.controller,
      this.obscureText,
      this.validator,
      this.editingComplete,
      this.onChanged,
      this.textInputType,
      this.textInputAction,
      this.focusNode,
      this.labelText,
      this.textColor = AppColorsManger.black,
      this.labelColor = AppColorsManger.grey,
      this.onFilledSubmited,
      this.initialValue,
      this.maxLines,
      this.prefixIcon,
      this.hintText,
      this.outlinedBorder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onFieldSubmitted: onFilledSubmited,
      cursorColor: AppColorsManger.mainAppColor,
      validator: validator,
      controller: controller,
      focusNode: focusNode,
      obscureText: obscureText ?? false,
      onChanged: onChanged,
      onEditingComplete: editingComplete,
      keyboardType: textInputType,
      textInputAction: textInputAction,
      maxLines: maxLines,
      initialValue: initialValue,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(
          color: AppColorsManger.grey,
          fontSize: 16.sp,
          fontWeight: FontWeight.normal,
        ),
        suffixIcon: suffixIcon,
        contentPadding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
        prefixIcon: prefixIcon,
        focusedErrorBorder: outlinedBorder == null || outlinedBorder == false
            ? const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.red,
                ),
              )
            : const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.red,
                ),
              ),
        errorBorder: outlinedBorder == null || outlinedBorder == false
            ? const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.red,
                ),
              )
            : const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.red,
                ),
              ),
        enabledBorder: outlinedBorder == null || outlinedBorder == false
            ? const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.black26,
                ),
              )
            : const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.black26,
                ),
              ),
        focusedBorder: outlinedBorder == null || outlinedBorder == false
            ? const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.mainAppColor,
                ),
              )
            : const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColorsManger.mainAppColor,
                ),
              ),
        labelText: labelText,
        labelStyle: TextStyle(
          color: labelColor,
          fontSize: 16.sp,
          fontWeight: FontWeight.bold,
        ),
      ),
      style: GoogleFonts.cairo(
        color: AppColorsManger.black,
        fontSize: 16.sp,
      ),
    );
  }
}
