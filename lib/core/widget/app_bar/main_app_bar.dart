import 'package:abd_samakeh_test/core/resources/font_size_manger.dart';
import 'package:abd_samakeh_test/generated/l10n.dart';
import 'package:flutter/material.dart';

import '../textUtils/text_utils.dart';

AppBar mainAppBar({
  required BuildContext context,
}) {
  return AppBar(
    title: TextUtils(text: S().homePage, fontSize: FontSizeManger.s16),
    elevation: 0,
    centerTitle: true,
  );
}
