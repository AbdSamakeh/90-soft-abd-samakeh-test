import 'package:abd_samakeh_test/core/resources/font_size_manger.dart';
import 'package:flutter/material.dart';

import '../textUtils/text_utils.dart';

AppBar secandoryAppBar({required BuildContext context, required String title}) {
  return AppBar(
    title: TextUtils(text: title, fontSize: FontSizeManger.s16),
    elevation: 0,
    automaticallyImplyLeading: true,
    centerTitle: true,
  );
}
