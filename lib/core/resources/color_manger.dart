import 'package:flutter/material.dart';

class AppColorsManger {
  // All Colors That I Use It In My Application ..
  static const Color mainAppColor = Color(0xff4ACFAC);
  static const Color secandoryColor = Color.fromARGB(255, 223, 223, 223);
  static const Color white = Color(0xFFF6F6F6);
  static const Color black = Colors.black;
  static const Color black26 = Colors.black26;
  static const Color red = Colors.red;
  static const Color grey = Colors.grey;
}

//Converter Function From Hex
extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
