// ignore_for_file: non_constant_identifier_names, constant_identifier_names

class AppKeysManger {
  // All Keys That I Use It In My Application ..
  static const String LANGUAGE = 'Language';
  static const String Updated = 'Updated';
  static const String DB_NAME = 'users';
  static const String ARABIC_KEY = 'ar';
  static const String ENGLISH_KEY = 'en';
}
