import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../generated/l10n.dart';
import '../resources/color_manger.dart';
import '../widget/textUtils/text_utils.dart';

class NotFoundScreen extends StatelessWidget {
  const NotFoundScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: 100.h,
        width: 100.w,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextUtils(
                text: S().notFound,
                fontSize: 24.sp,
                fontWeight: FontWeight.bold,
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: TextUtils(
                  text: S().back,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold,
                  color: AppColorsManger.mainAppColor,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
