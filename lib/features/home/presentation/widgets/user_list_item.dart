import 'package:abd_samakeh_test/features/user/presentation/bloc/user_cubit/user_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:abd_samakeh_test/core/resources/font_size_manger.dart';
import 'package:abd_samakeh_test/core/widget/textUtils/text_utils.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../core/resources/color_manger.dart';
import '../../../../core/resources/keys_manger.dart';
import '../../../../generated/l10n.dart';
import '../../../../router/app_router.dart';
import '../../../user/domain/entities/response/user_entite.dart';
import '../../../user/presentation/bloc/delete_user_cubit/delete_user_cubit.dart';

class UserListITem extends StatelessWidget {
  const UserListITem({
    Key? key,
    required this.userDetails,
  }) : super(key: key);
  final User userDetails;
  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: Key(userDetails.id.toString()),
      endActionPane: ActionPane(
        motion: const DrawerMotion(),
        dismissible: DismissiblePane(onDismissed: () {
          // we can able to perform to some action here
        }),
        children: [
          SlidableAction(
            autoClose: true,
            flex: 1,
            onPressed: (value) {
              context
                  .read<DeleteUserCubit>()
                  .deletUser(userId: userDetails.id!);
              context.read<UserCubit>().getUsers();
            },
            backgroundColor: AppColorsManger.red,
            foregroundColor: AppColorsManger.secandoryColor,
            icon: Icons.delete,
            label: S().delete,
          ),
        ],
      ),
      child: Card(
        child: ListTile(
          leading: const Icon(Icons.person),
          title: TextUtils(
              text: userDetails.name ?? '',
              fontSize: FontSizeManger.s14,
              color: AppColorsManger.black),
          subtitle: TextUtils(
            text: userDetails.email ?? '',
            fontSize: FontSizeManger.s14,
            color: AppColorsManger.black,
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                onPressed: () async {
                  //Save Result To Update List If User Updated
                  final result = await Navigator.pushNamed(
                      context, RouteNamedScreens.editUserPageNameRoute,
                      arguments: userDetails);
                  if (result == AppKeysManger.Updated) {
                    // ignore: use_build_context_synchronously
                    context.read<UserCubit>().getUsers();
                  }
                },
                icon: Icon(
                  Icons.edit,
                  size: 4.w,
                  color: Colors.teal,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
