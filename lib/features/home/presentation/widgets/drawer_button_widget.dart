import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../core/resources/color_manger.dart';
import '../../../../core/widget/textUtils/text_utils.dart';

class DrawerButton extends StatelessWidget {
  const DrawerButton({
    Key? key,
    required this.icon,
    required this.text,
    required this.onTap,
  }) : super(key: key);
  final IconData icon;
  final void Function() onTap;
  final String text;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        height: 7.h,
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: AppColorsManger.mainAppColor),
            bottom: BorderSide(color: AppColorsManger.mainAppColor),
          ),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextUtils(
              text: text,
              fontSize: 18.sp,
              fontWeight: FontWeight.w400,
            ),
            Icon(
              icon,
              size: 7.w,
              color: AppColorsManger.mainAppColor,
            )
          ],
        ),
      ),
    );
  }
}
