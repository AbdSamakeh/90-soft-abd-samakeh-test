import 'package:abd_samakeh_test/core/resources/enum_manger.dart';
import 'package:abd_samakeh_test/core/resources/font_size_manger.dart';
import 'package:abd_samakeh_test/core/resources/keys_manger.dart';
import 'package:abd_samakeh_test/core/widget/textUtils/text_utils.dart';
import 'package:abd_samakeh_test/features/user/presentation/bloc/user_cubit/user_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../core/resources/color_manger.dart';
import '../../../../core/theme/localization/localization_bloc.dart';
import '../../../../core/widget/app_bar/main_app_bar.dart';
import '../../../../generated/l10n.dart';
import '../../../../router/app_router.dart';
import '../../../user/domain/entities/response/user_entite.dart';
import '../widgets/drawer_button_widget.dart';
import '../widgets/user_list_item.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          backgroundColor: AppColorsManger.secandoryColor,
          elevation: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              DrawerButton(
                icon: Icons.language,
                text: 'العربية',
                onTap: () {
                  context.read<LocalizationBloc>().add(
                      const ChangeLanguage(language: AppKeysManger.ARABIC_KEY));
                  Navigator.pop(context);
                  setState(() {});
                },
              ),
              DrawerButton(
                icon: Icons.language,
                text: 'English',
                onTap: () {
                  context.read<LocalizationBloc>().add(const ChangeLanguage(
                      language: AppKeysManger.ENGLISH_KEY));
                  Navigator.pop(context);
                  setState(() {});
                },
              ),
            ],
          )),
      backgroundColor: AppColorsManger.secandoryColor,
      appBar: mainAppBar(context: context),
      body: BlocConsumer<UserCubit, UserState>(
        listener: (context, state) {},
        builder: (context, state) {
          switch (state.status) {
            case DeafultBlocStatus.done:
              return Visibility(
                visible: state.users.isNotEmpty,
                replacement: Center(
                  child: TextUtils(
                      text: S().thereIsNoUsersYet,
                      fontSize: FontSizeManger.s14),
                ),
                child: RefreshIndicator(
                  onRefresh: () async {
                    context.read<UserCubit>().getUsers();
                  },
                  child: ListView.builder(
                    itemCount: state.users.length,
                    itemBuilder: (context, index) {
                      final User userDetails = state.users[index];
                      return UserListITem(
                        userDetails: userDetails,
                      );
                    },
                    shrinkWrap: true,
                  ),
                ),
              );
            case DeafultBlocStatus.loading:
              return const Center(
                child: CircularProgressIndicator(),
              );
            default:
              {
                return const SizedBox();
              }
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          //Save Result To Update List If User Added
          final result = await Navigator.pushNamed(
              context, RouteNamedScreens.createUserPageNameRoute);
          if (result == AppKeysManger.Updated) {
            // ignore: use_build_context_synchronously
            context.read<UserCubit>().getUsers();
          }
        },
        child: Icon(
          Icons.add,
          size: 4.w,
        ),
      ),
    );
  }
}
