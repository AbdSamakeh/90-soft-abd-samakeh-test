import 'package:abd_samakeh_test/core/resources/color_manger.dart';
import 'package:flutter/material.dart';

import '../../../../core/resources/font_size_manger.dart';
import '../../../../core/widget/textUtils/text_utils.dart';

class TextButtonWidget extends StatelessWidget {
  const TextButtonWidget({
    Key? key,
    this.onPressed,
    required this.title,
    required this.foregroundColor,
    required this.backgroundColor,
  }) : super(key: key);
  final void Function()? onPressed;
  final String title;
  final Color foregroundColor;
  final Color backgroundColor;
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
          foregroundColor: foregroundColor,
          backgroundColor: backgroundColor,
          textStyle: const TextStyle(fontSize: 15)),
      onPressed: onPressed,
      child: TextUtils(
        text: title,
        fontSize: FontSizeManger.s14,
        color: AppColorsManger.white,
      ),
    );
  }
}
