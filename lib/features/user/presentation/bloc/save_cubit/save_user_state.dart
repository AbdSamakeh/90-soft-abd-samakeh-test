part of 'save_user_cubit.dart';

class SaveUserState extends Equatable {
  final DeafultBlocStatus status;
  final String error;
  const SaveUserState({
    required this.status,
    required this.error,
  });

  factory SaveUserState.initial() {
    return const SaveUserState(status: DeafultBlocStatus.initial, error: '');
  }

  SaveUserState copyWith({
    DeafultBlocStatus? status,
    String? error,
  }) {
    return SaveUserState(
      status: status ?? this.status,
      error: error ?? this.error,
    );
  }

  @override
  List<Object> get props => [status, error];
}
