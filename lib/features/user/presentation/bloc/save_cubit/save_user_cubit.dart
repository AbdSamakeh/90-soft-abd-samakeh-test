import 'package:abd_samakeh_test/core/resources/enum_manger.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../domain/entities/response/user_entite.dart';
import '../../../domain/usecases/save_user_usecase.dart';

part 'save_user_state.dart';

class SaveUserCubit extends Cubit<SaveUserState> {
  final SaveUserUsecase saveUserUsecase;
  SaveUserCubit({required this.saveUserUsecase})
      : super(SaveUserState.initial());
  //Save User
  saveUser({required User user}) async {
    emit(state.copyWith(status: DeafultBlocStatus.loading));
    await saveUserUsecase.saveUser(user: user);
    emit(state.copyWith(status: DeafultBlocStatus.done));
  }
}
