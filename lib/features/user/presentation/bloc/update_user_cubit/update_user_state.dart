part of 'update_user_cubit.dart';

class UpdateUserState extends Equatable {
  final String error;
  final DeafultBlocStatus status;
  const UpdateUserState({
    required this.error,
    required this.status,
  });

  factory UpdateUserState.initial() {
    return const UpdateUserState(error: '', status: DeafultBlocStatus.initial);
  }
  UpdateUserState copyWith({
    String? error,
    DeafultBlocStatus? status,
  }) {
    return UpdateUserState(
      error: error ?? this.error,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [error, status];
}
