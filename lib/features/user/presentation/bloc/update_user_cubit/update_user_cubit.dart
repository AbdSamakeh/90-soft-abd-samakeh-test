import 'package:abd_samakeh_test/core/resources/enum_manger.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:abd_samakeh_test/features/user/domain/usecases/update_user_usecase.dart';

import '../../../domain/entities/response/user_entite.dart';

part 'update_user_state.dart';

class UpdateUserCubit extends Cubit<UpdateUserState> {
  final UpdateUserUsecase updateUserUsecase;
  UpdateUserCubit({
    required this.updateUserUsecase,
  }) : super(UpdateUserState.initial());
  //Update User
  updateUser({required User user}) async {
    emit(state.copyWith(status: DeafultBlocStatus.loading));
    await updateUserUsecase.updateUser(user: user);
    emit(state.copyWith(status: DeafultBlocStatus.done));
  }
}
