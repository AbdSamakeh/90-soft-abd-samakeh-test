part of 'delete_user_cubit.dart';

class DeleteUserState extends Equatable {
  final String error;
  final DeafultBlocStatus status;
  const DeleteUserState({
    required this.error,
    required this.status,
  });

  factory DeleteUserState.initial() {
    return const DeleteUserState(error: '', status: DeafultBlocStatus.initial);
  }

  DeleteUserState copyWith({
    String? error,
    DeafultBlocStatus? status,
  }) {
    return DeleteUserState(
      error: error ?? this.error,
      status: status ?? this.status,
    );
  }

  @override
  String toString() => 'DeleteUserState(error: $error, status: $status)';

  @override
  List<Object> get props => [error, status];
}
