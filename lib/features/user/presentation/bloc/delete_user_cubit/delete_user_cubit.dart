import 'package:abd_samakeh_test/core/resources/enum_manger.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../domain/usecases/delete_user_usecase.dart';

part 'delete_user_state.dart';

class DeleteUserCubit extends Cubit<DeleteUserState> {
  final DeleteUserUsecase deleteUserUsecase;
  DeleteUserCubit({required this.deleteUserUsecase})
      : super(DeleteUserState.initial());
  //Delete Usesr By User Id
  deletUser({required int userId}) async {
    emit(state.copyWith(status: DeafultBlocStatus.loading));
    await deleteUserUsecase.deleteUser(userId: userId);
    emit(state.copyWith(status: DeafultBlocStatus.done));
  }
}
