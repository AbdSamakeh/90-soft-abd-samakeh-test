import 'package:abd_samakeh_test/core/resources/enum_manger.dart';
import 'package:abd_samakeh_test/features/user/domain/entities/response/user_entite.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../domain/usecases/get_users_usecase.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final GetUsersUsecase getUsersUsecase;
  UserCubit({required this.getUsersUsecase}) : super(UserState.initial());
  //Get Users
  getUsers() async {
    late List<User> userList = <User>[];
    emit(state.copyWith(status: DeafultBlocStatus.loading));
    final users = await getUsersUsecase.getUsers();
    //Casting Result Query
    userList = <User>[];
    users.forEach((user) {
      var userModel = User();
      userModel.id = user['id'];
      userModel.name = user['name'];
      userList.add(userModel);
    });
    emit(state.copyWith(status: DeafultBlocStatus.done, users: userList));
  }
}
