part of 'user_cubit.dart';

class UserState extends Equatable {
  final String? error;
  final DeafultBlocStatus status;
  final List<User> users;
  const UserState({
    this.error,
    required this.status,
    required this.users,
  });

  factory UserState.initial() {
    return const UserState(status: DeafultBlocStatus.initial, users: []);
  }

  UserState copyWith({
    String? error,
    DeafultBlocStatus? status,
    List<User>? users,
  }) {
    return UserState(
      error: error ?? this.error,
      status: status ?? this.status,
      users: users ?? this.users,
    );
  }

  @override
  List<Object> get props => [ status, users];
}
