import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'package:abd_samakeh_test/core/resources/color_manger.dart';
import 'package:abd_samakeh_test/core/resources/enum_manger.dart';
import 'package:abd_samakeh_test/core/resources/font_size_manger.dart';
import 'package:abd_samakeh_test/core/resources/keys_manger.dart';
import 'package:abd_samakeh_test/core/widget/loading/main_circular_progress_Indicator_widget.dart';
import 'package:abd_samakeh_test/core/widget/noteMessage/note_message.dart';
import 'package:abd_samakeh_test/core/widget/textUtils/text_utils.dart';
import 'package:abd_samakeh_test/generated/l10n.dart';

import '../../../../core/widget/app_bar/secandory_app_bar.dart';
import '../../../../core/widget/formField/app_form_field.dart';
import '../../domain/entities/response/user_entite.dart';
import '../bloc/update_user_cubit/update_user_cubit.dart';
import '../widgets/text_button_widget.dart';

class UpdateUserPage extends StatelessWidget {
  const UpdateUserPage({
    Key? key,
    required this.userDetails,
  }) : super(key: key);

  final User userDetails;
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> key = GlobalKey();
    return Scaffold(
      appBar: secandoryAppBar(context: context, title: S().createUser),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: key,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextUtils(
                  text: S().updateUser,
                  fontSize: FontSizeManger.s16,
                ),
                SizedBox(
                  height: 5.h,
                ),
                //User Name
                AppTextFormField(
                  onChanged: (value) {
                    userDetails.name = value;
                    return null;
                  },
                  initialValue: userDetails.name,
                  textInputType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return S().thisFiledIsRequired;
                    }
                    return null;
                  },
                  hintText: S().enterName,
                  labelText: S().name,
                ),
                SizedBox(
                  height: 5.h,
                ),
                //User Email
                AppTextFormField(
                  onChanged: (value) {
                    userDetails.email = value;
                    return null;
                  },
                  initialValue: userDetails.email,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.emailAddress,
                  validator: (value) {
                    final bool emailValid = RegExp(
                            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                        .hasMatch(value!);
                    if (value.isEmpty) {
                      return S().thisFiledIsRequired;
                    }
                    if (!emailValid) {
                      return 'email@example.com';
                    }
                    return null;
                  },
                  hintText: S().enterEmail,
                  labelText: S().email,
                ),
                SizedBox(
                  height: 5.h,
                ),
                //Submit Button
                BlocConsumer<UpdateUserCubit, UpdateUserState>(
                  listener: (context, state) {
                    switch (state.status) {
                      case DeafultBlocStatus.done:
                        Navigator.pop(context, AppKeysManger.Updated);
                        NoteMessage.showSnackBar(
                            context: context,
                            text: S().processSuccsesful,
                            warning: false);
                        break;
                      case DeafultBlocStatus.error:
                        NoteMessage.showSnackBar(
                            context: context, text: state.error, warning: true);
                        break;
                      default:
                    }
                  },
                  builder: (context, state) {
                    switch (state.status) {
                      case DeafultBlocStatus.loading:
                        return const Center(
                          child: MainCircularProgressIndicator(),
                        );
                      default:
                        {
                          return TextButtonWidget(
                              foregroundColor: AppColorsManger.white,
                              backgroundColor: AppColorsManger.mainAppColor,
                              onPressed: () {
                                if (key.currentState!.validate()) {
                                  context
                                      .read<UpdateUserCubit>()
                                      .updateUser(user: userDetails);
                                }
                              },
                              title: S().updateUser);
                        }
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
