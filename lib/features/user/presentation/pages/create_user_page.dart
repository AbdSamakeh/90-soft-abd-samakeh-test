import 'package:abd_samakeh_test/core/resources/color_manger.dart';
import 'package:abd_samakeh_test/core/resources/enum_manger.dart';
import 'package:abd_samakeh_test/core/resources/font_size_manger.dart';
import 'package:abd_samakeh_test/core/resources/keys_manger.dart';
import 'package:abd_samakeh_test/core/widget/loading/main_circular_progress_Indicator_widget.dart';
import 'package:abd_samakeh_test/core/widget/noteMessage/note_message.dart';
import 'package:abd_samakeh_test/core/widget/textUtils/text_utils.dart';
import 'package:abd_samakeh_test/features/user/presentation/bloc/save_cubit/save_user_cubit.dart';
import 'package:abd_samakeh_test/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../core/widget/app_bar/secandory_app_bar.dart';
import '../../../../core/widget/formField/app_form_field.dart';
import '../../domain/entities/response/user_entite.dart';
import '../widgets/text_button_widget.dart';

class CreateUserPage extends StatelessWidget {
  const CreateUserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> key = GlobalKey();
    final userNameController = TextEditingController();
    final userEmailController = TextEditingController();
    return Scaffold(
      appBar: secandoryAppBar(context: context, title: S().createUser),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: key,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextUtils(
                  text: S().addNewUser,
                  fontSize: FontSizeManger.s16,
                ),
                SizedBox(
                  height: 5.h,
                ),
                //User Name
                AppTextFormField(
                  textInputType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return S().thisFiledIsRequired;
                    }
                    return null;
                  },
                  controller: userNameController,
                  hintText: S().enterName,
                  labelText: S().name,
                ),
                SizedBox(
                  height: 5.h,
                ),
                //User Email
                AppTextFormField(
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.emailAddress,
                  validator: (value) {
                    final bool emailValid = RegExp(
                            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                        .hasMatch(value!);
                    if (value.isEmpty) {
                      return S().thisFiledIsRequired;
                    }
                    if (!emailValid) {
                      return 'email@example.com';
                    }
                    return null;
                  },
                  controller: userEmailController,
                  hintText: S().enterEmail,
                  labelText: S().email,
                ),
                SizedBox(
                  height: 5.h,
                ),
                //Submit Button
                BlocConsumer<SaveUserCubit, SaveUserState>(
                  listener: (context, state) {
                    switch (state.status) {
                      case DeafultBlocStatus.done:
                        Navigator.pop(context, AppKeysManger.Updated);
                        NoteMessage.showSnackBar(
                            context: context,
                            text: S().processSuccsesful,
                            warning: false);
                        break;
                      case DeafultBlocStatus.error:
                        NoteMessage.showSnackBar(
                            context: context, text: state.error, warning: true);
                        break;
                      default:
                    }
                  },
                  builder: (context, state) {
                    switch (state.status) {
                      case DeafultBlocStatus.loading:
                        return const Center(
                          child: MainCircularProgressIndicator(),
                        );
                      default:
                        {
                          return TextButtonWidget(
                              foregroundColor: AppColorsManger.white,
                              backgroundColor: AppColorsManger.mainAppColor,
                              onPressed: () {
                                if (key.currentState!.validate()) {
                                  User userData = User();
                                  userData.email =
                                      userEmailController.text.trim();
                                  userData.name =
                                      userNameController.text.trim();
                                  context
                                      .read<SaveUserCubit>()
                                      .saveUser(user: userData);
                                }
                              },
                              title: S().createUser);
                        }
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
