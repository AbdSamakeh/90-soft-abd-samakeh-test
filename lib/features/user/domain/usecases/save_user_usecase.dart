import '../entities/response/user_entite.dart';
import '../repositories/user_repository.dart';

class SaveUserUsecase {
  final UserRepository userRepository;
  SaveUserUsecase({
    required this.userRepository,
  });

  saveUser({required User user}) async {
    return await userRepository.saveUser(user: user);
  }
}
