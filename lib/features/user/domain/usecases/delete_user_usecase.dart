import '../repositories/user_repository.dart';

class DeleteUserUsecase {
  final UserRepository userRepository;
  DeleteUserUsecase({
    required this.userRepository,
  });

  deleteUser({required int userId}) async {
    return userRepository.deleteUser(userId: userId);
  }
}
