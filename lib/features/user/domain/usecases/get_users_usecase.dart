import '../repositories/user_repository.dart';

class GetUsersUsecase {
  final UserRepository userRepository;
  GetUsersUsecase({
    required this.userRepository,
  });

  getUsers() async {
    return await userRepository.getAllUsers();
  }
}
