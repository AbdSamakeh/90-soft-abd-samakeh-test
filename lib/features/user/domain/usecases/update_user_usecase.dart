import '../entities/response/user_entite.dart';
import '../repositories/user_repository.dart';

class UpdateUserUsecase {
  final UserRepository userRepository;
  UpdateUserUsecase({
    required this.userRepository,
  });

  updateUser({required User user}) async {
    return await userRepository.updateUser(user: user);
  }
}
