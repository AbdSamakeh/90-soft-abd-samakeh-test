class User{
	int? id;
	String? name;
	String? email;
	userMap(id, name , email) {
		var mapping = <String, dynamic>{};
		// ignore: unnecessary_null_in_if_null_operators
		mapping['id'] = id ?? null;
		mapping['name'] = name!;
		mapping['email'] = email!;
		return mapping;
	  }
}