import 'package:abd_samakeh_test/core/resources/keys_manger.dart';

import '../../../../db_helper/db_repository.dart';
import '../entities/response/user_entite.dart';

class UserRepository {
  late Repository _repository;
  UserRepository() {
    _repository = Repository();
  }
  //Save User
  saveUser({required User user}) async {
    return await _repository.insertData(AppKeysManger.DB_NAME, user.userMap(user.id , user.name, user.email));
  }

  //Get All Users
  getAllUsers() async {
    return await _repository.readData(AppKeysManger.DB_NAME);
  }

  //Update User
  updateUser({required User user}) async {
    return await _repository.updateData(AppKeysManger.DB_NAME, user.userMap(user.id , user.name ,user.email ));
  }

  //Delete User
  deleteUser({required int userId}) async {
    return await _repository.deleteDataById(AppKeysManger.DB_NAME, userId);
  }
}
