import 'package:sqflite/sqflite.dart';

import 'data_base_connection.dart';

	class Repository {
		late DatabaseConnection _databaseConnection;
		Repository() {
			_databaseConnection= DatabaseConnection();
		  }
		static Database? _database;
		Future<Database?>get database async {
			if (_database != null) {
				return _database;
			} else {
				_database = await _databaseConnection.setDatabase();
				return _database;
			}
		}

    //Insert User
		insertData(table, data) async {
			var connection = await database;
			return await connection?.insert(table, data);
		}

    //Get All Users
		readData(table) async {
			var connection = await database;
			return await connection?.query(table);
		}

    //Update User By ID
		updateData(table, data) async {
			var connection = await database;
			return await connection
			?.update(table, data, where: 'id=?', whereArgs: [data['id']]);
		}

    //Delete User By User ID
		deleteDataById(table, itemId) async {
			var connection = await database;
			return await connection?.rawDelete("delete from $table where id=$itemId");
		}
	}