import 'package:sqflite/sqflite.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

import '../core/resources/keys_manger.dart';

class DatabaseConnection {
  Future<Database> setDatabase() async {
    var directory = await getApplicationDocumentsDirectory();
    var path = join(directory.path, 'db_user');
    var database =
        await openDatabase(path, version: 1, onCreate: _createDatabase);
    return database;
  }

  Future<void> _createDatabase(Database database, int version) async {
    String sqlStatement =
        "CREATE TABLE ${AppKeysManger.DB_NAME} (id INTEGER PRIMARY KEY,name TEXT,email TEXT);";
    await database.execute(sqlStatement);
  }
}
